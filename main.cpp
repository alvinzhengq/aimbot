#include <opencv2\opencv.hpp>
#include "./lib/Serial.h"
#include "./lib/PID.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <string.h>
#include <thread>
#include <cmath>

using namespace cv;
using namespace std;
typedef std::chrono::system_clock Clock;

SerialPort *SP;

const String capture_window = "Video Capture";
const String mask_window = "Object Detection";

const int lowH = 0, highH = 180, lowS = 216, highS = 255, lowV = 162, highV = 255;
const Mat element = getStructuringElement(MORPH_RECT, Size(2 * 4 + 1, 2 * 4 + 1), Point(4, 4));

double x_pos = 0, y_pos = 0;
auto start = std::chrono::time_point_cast<std::chrono::seconds>(Clock::now());
auto lastClick = std::chrono::time_point_cast<std::chrono::seconds>(Clock::now());

double get_x_pos() { return x_pos; }
double get_y_pos() { return y_pos; }

union
{
    char buf[4];
    int16_t num;
} number;

void send_data_thread(int data)
{
    DWORD bytesSent;

    WriteFile(SP->handler, &data, sizeof(data), &bytesSent, NULL);
    ClearCommError(SP->handler, &SP->errors, &SP->status);
}

void x_pid_output(double out) { send_data_thread((int)(out + 1000)); }
void y_pid_output(double out) { send_data_thread((int)(out - 1000)); }

unsigned long get_time()
{
    auto fraction = Clock::now() - start;
    return (unsigned long)(std::chrono::duration_cast<std::chrono::milliseconds>(fraction).count());
}

int main()
{
    Mat image, hsvMask, thresholdMask, dilateMask;
    namedWindow(capture_window);
    namedWindow(mask_window);

    VideoCapture cap(2, 700);
    cap.set(CAP_PROP_FRAME_WIDTH, 1200);
    cap.set(CAP_PROP_FRAME_HEIGHT, 630);

    if (!cap.isOpened())
    {
        std::cout << "Failed to Open Camera";
        exit(0);
    }

    PIDController<double> pid_x(2.6, 0, 34, get_x_pos, x_pid_output);
    PIDController<double> pid_y(2.6, 0, 34, get_y_pos, y_pid_output);

    pid_x.registerTimeFunction(get_time);
    pid_y.registerTimeFunction(get_time);

    pid_x.setMaxIntegralCumulation(100);
    pid_x.setOutputBounds(-600, 600);
    pid_x.setOutputBounded(true);

    pid_y.setMaxIntegralCumulation(100);
    pid_y.setOutputBounded(true);
    pid_y.setOutputBounds(-600, 600);

    SP = new SerialPort("\\\\.\\COM3");

    while (true)
    {
        cap >> image;

        cvtColor(image, hsvMask, COLOR_BGR2HSV);
        inRange(hsvMask, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), thresholdMask);

        dilate(thresholdMask, dilateMask, element);

        Mat result, result_resized;
        bitwise_and(image, image, result, dilateMask);

        vector<vector<Point>> contours;
        vector<Vec4i> hierarchy;
        findContours(dilateMask, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

        Point2f closest = Point2f(0, 0);
        vector<Point> closest_contour = {Point2f(0, 0)};
        Point2f center = Point2f(image.size().width / 2, image.size().height / 2);

        for (int i = 0; i < contours.size(); i++)
        {
            double area = contourArea(contours[i]);

            if (area > 1000)
            {
                Rect b = boundingRect(contours[i]);
                Point2f contour_center = (b.tl() + b.br()) * 0.5;

                circle(result, contour_center, 10, Scalar(255, 255, 255), 2, 2);
                circle(result, center, 10, Scalar(255, 0, 0), 2, 2);

                if (cv::norm(center - contour_center) < cv::norm(center - closest))
                {
                    closest = contour_center;
                    closest_contour = contours[i];
                }
            }
        }

        cv::line(result, center, closest, Scalar(255, 255, 255), 2, 2);
        if (pointPolygonTest(closest_contour, center, true) > 8 &&
            (unsigned long)(std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - lastClick).count() >= 600))
        {
            send_data_thread(0);
            lastClick = std::chrono::time_point_cast<std::chrono::seconds>(Clock::now());
        }

        // ####################################################################################

        x_pos = closest.x;
        y_pos = closest.y;

        pid_x.setTarget(center.x);
        pid_y.setTarget(center.y);

        pid_x.tick();
        pid_y.tick();

        // ####################################################################################

        cv::resize(result, result_resized, Size(1920, 1080), 1, 1, 2);
        cv::imshow(capture_window, result_resized);
        cv::imshow(mask_window, image);

        if (waitKey(1) >= 0)
            break;
    }

    return 0;
}
