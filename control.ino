#include <Stepper.h>
#include <MemoryFree.h>

// Define a stepper and the pins it will use
Stepper mot1(200, 2, 3); // bot left
Stepper mot2(200, 4, 5); // bot right
Stepper mot3(200, 6, 7); // top right
Stepper mot4(200, 8, 9); // top left

Stepper allMots[4] = {mot1, mot2, mot3, mot4};
const int8_t motDir[4][4] = {
    {1, 1, 1, 1},
    {-1, -1, -1, -1},
    {1, -1, 1, -1}, // right
    {-1, 1, -1, 1}, // left
};

void setup()
{
    Serial.begin(9600);
}

uint16_t motXSpeed = 1, motYSpeed = 1;
int8_t stepsX = 0, stepsY = 0;
int8_t dirXMag = 1, dirYMag = 1;

union
{
    char buf[4];
    int16_t num;
} data;

void loop()
{
    for (uint8_t i = 0; i < 4; i++)
    {
        int16_t netForce = ((dirXMag > 0) ? motDir[2][i] : motDir[3][i]) * motXSpeed * stepsX
                        + ((dirYMag > 0) ? motDir[0][i] : motDir[1][i]) * motYSpeed * stepsY;
        
        allMots[i].setSpeed(
            min(50, max(1,
              abs(netForce)
            ))
        );
        allMots[i].step(
            ((netForce > 0) - (netForce < 0)) 
            * ((stepsX != 0 || stepsY != 0) ? 1 : 0)
        );
    }

    while (Serial.available() > 0)
    {
        Serial.readBytes(data.buf, 4);

        if (data.num >= 400)
        {
            int16_t s = data.num - 1000;

            motXSpeed = map(abs(s), 0, 601, 1, 30);
            dirXMag = (s > 0) ? 1 : -1;
            stepsX = (s == 0) ? 0 : 1;
        }

        if (data.num <= -400)
        {
            int16_t s = data.num + 1000;

            motYSpeed = map(abs(s), 0, 601, 1, 30);
            dirYMag = (s > 0) ? 1 : -1;
            stepsY = (s == 0) ? 0 : 1;
        }

        for (uint8_t i = 0; i < 4; i++)
        {
            int16_t netForce = ((dirXMag > 0) ? motDir[2][i] : motDir[3][i]) * motXSpeed * stepsX
                            + ((dirYMag > 0) ? motDir[0][i] : motDir[1][i]) * motYSpeed * stepsY;
    
            data.num = 
            (
              ((netForce > 0) - (netForce < 0)) 
              * ((stepsX != 0 || stepsY != 0) ? 1 : 0)
            ) 
            * 
            (
              min(50, max(1,
                abs(netForce)
              ))
            );
            
            Serial.write(data.buf, 4);
        }
    }
}