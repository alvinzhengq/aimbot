# High Precision Omni-Directional Aim-Bot

#### File Directory

- **main.cpp**
    - Main Image Processing, PID input/output code, and Serial Communication with Arduino
- **control.ino**
    - Code running on Arduino Uno, handles analog motor movement
- **lib/PID.***
    - Simple multi-platform C++ PID library by https://github.com/nicholastmosher/PID
- **lib/Serial.***
    - C++ Serial Library from Arduino, allows for PC-Arduino Serial Communication